import firebase from 'firebase/compat/app'
import "firebase/compat/auth";
import "firebase/compat/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDdtldW2VGxHVcXP-c1GSTPaPUOpCemaCw",
  authDomain: "imessage-clone-32cb8.firebaseapp.com",
  projectId: "imessage-clone-32cb8",
  storageBucket: "imessage-clone-32cb8.appspot.com",
  messagingSenderId: "749894237322",
  appId: "1:749894237322:web:6f46f3d33800148ad30173",
  measurementId: "G-F1YZ758CVB"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { auth, provider };
export default db;