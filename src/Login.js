import { Button } from "@material-ui/core";
import React from "react";
import "./Login.css";
import { auth, provider } from './tes-firebase'

function Login() {
    const singIn = () => {
        auth.signInWithPopup(provider).catch((error) => alert(error.message));
    }

    return (
        <div className="login">
            <div className="login__logo">
                <img 
                    src="https://th.bing.com/th/id/R.dc0d3ad682ab29fda1face34ac066199?rik=of806rJrzgwaWg&riu=http%3a%2f%2fwww.newdesignfile.com%2fpostpic%2f2014%2f03%2fimessage-icon_27015.png&ehk=HhT64XTxfFiTiekQ8Fbfg%2bB262w9SHFMl%2fODkN%2bD48E%3d&risl=&pid=ImgRaw&r=0"
                    alt="login__logo"
                />
                <h1>iMessage</h1>
            </div>

            <Button onClick={singIn}>Sing In</Button>
        </div>
    )
}

export default Login